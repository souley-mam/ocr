import cv2
import numpy as np

img = cv2.imread('image.jpg')

class NettoyageImage:


        def __init__(self, image, template):
            self.image = image
            self.template = template

        # get grayscale image
        def get_grayscale(self):
            self.image = image
            return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


        # noise removal
        def remove_noise(self):
            self.image = image
            return cv2.medianBlur(image, 5)


        # thresholding
        def thresholding(self):
            self.image = image
            return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


        # dilation
        def dilate(self):
            self.image = image
            kernel = np.ones((5, 5), np.uint8)
            return cv2.dilate(image, kernel, iterations=1)

        #erosion
        def erode(self):
            self.image = image
            kernel = np.ones((5,5),np.uint8)
            return cv2.erode(image, kernel, iterations = 1)

        #opening - erosion followed by dilation
        def opening(self):
            self.image = image
            kernel = np.ones((5,5),np.uint8)
            return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

        #canny edge detection
        def canny(self):
            self.image = image
            return cv2.Canny(image, 100, 200)

        #skew correction
        def deskew(image):
            coords = np.column_stack(np.where(image > 0))
            angle = cv2.minAreaRect(coords)[-1]
            if angle < -45:
                 angle = -(90 + angle)
            else:
                 angle = -angle
            (h, w) = image.shape[:2]
            center = (w // 2, h // 2)
            M = cv2.getRotationMatrix2D(center, angle, 1.0)
            rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
            return rotated

        #template matching
        def match_template(self, template):
            self.image = image
            self.template = template
            return cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)


class NettoyageText:

    def read_book(self, title_path):
        self.title_path = title_path
        with open(title_path, "r", encoding="utf8") as current_file:
            text = current_file.read()
            text = text.replace("\n", "").replace("\r", "")
        return text

    def count_words_fast(self):
        self.text = text
        text = text.lower()
        skips = [".", ", ", ":", ";", "'", '"']
        for ch in skips:
            text = text.replace(ch, "")
        word_counts = Counter(text.split(" "))
        return word_counts

    def word_stats(self):
        self.word_counts = word_counts
        num_unique = len(word_counts)
        counts = word_counts.values()
        return (num_unique, counts)
